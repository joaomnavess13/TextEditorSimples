var showSourceCode = false;
var isInEditMode = true;


function enableEditorMode(){
    richTextField.document.designMode = "on";
}
function execCmd(command){
    richTextField.document.execCommand(command, false, null); 
}
function execCommandWithArg(command, arg){
    richTextField.document.execCommand(command, false, arg); 

}
function toggleSource(){

    if(showSourceCode){
        richTextField.document.getElementsByTagName('body')[0].innerHTML = richTextField.document.getElementsByTagName('body')[0].textContent;
        showSourceCode = false;
    }else{
        richTextField.document.getElementsByTagName('body')[0].textContent = richTextField.document.getElementsByTagName('body')[0].innerHTML;
        showSourceCode = true; 
    }

}
function toggleEdit(){
    if(isInEditMode){
        isInEditMode = false;
    }
    isInEditMode = true;
}